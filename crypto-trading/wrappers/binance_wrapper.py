from os import getenv
from binance.client import Client as binance_client

class BinanceWrapper:
    def __init__(self):
        self._binance_client = binance_client(getenv("dev-1-public"), getenv("dev-1-private"))
        # Client for the Spot, Futures or Vanilla Options Testnet
        self._binance_client_testnet = binance_client(getenv("dev-1-public"), getenv("dev-1-private"), testnet=True)

    # Function to get the binance API rate limits
    def get_API_rate_limits(self):
        return self._binance_client.get_exchange_info()

    # Function to get all the tickers available in Binance
    def get_tickers(self):
        return self._binance_client.get_all_tickers()

    # Function to search for a particular ticker and retrieve its price
    def search_tickers(self, tick):
        tickers = self.get_tickers()
        resp = [elem for elem in tickers if elem.get('symbol') == tick]
        return resp